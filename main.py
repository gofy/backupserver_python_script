#!/usr/bin/python
#
#  ____             _
# |  _ \           | |
# | |_) | __ _  ___| | ___   _ _ __  ___  ___ _ ____   _____ _ __
# |  _ < / _` |/ __| |/ / | | | '_ \/ __|/ _ \ '__\ \ / / _ \ '__|
# | |_) | (_| | (__|   <| |_| | |_) \__ \  __/ |   \ V /  __/ |
# |____/ \__,_|\___|_|\_\\__,_| .__/|___/\___|_|    \_/ \___|_|
#                             | |
#                             |_|
#
#     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
#     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
#     MMMM     MMMMMMM                MMMM
#     MMMM     MMMMMMM                MMMM
#     MMMM     MMMMMMM     MMMMMMM    MMMM
#     MMMM     MMMMMMM     MMMMMMM    MMMM
#     MMMM     MMMMMMM     MMMMMMM    MMMM
#     MMMM     MM              MMM    MMMM
#     MMMM     MMMM           MMMM    MMMM
#     MMMM     MMMMMM       MMMMMM    MMMM
#     MMMM     MMMMMMMM   MMMMMMMM    MMMM
#     MMMM      MMMMMMMM.MMMMMMMM     MMMM
#     MMMM                            MMMM
#     MMMM                            MMMM
#     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
#     MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
#  _             _           _
# | |           | |         | |
# | |__  _   _  | |    _   _| | _____
# | '_ \| | | | | |   | | | | |/ / _ \
# | |_) | |_| | | |___| |_| |   <  __/
# |_.__/ \__, | |______\__,_|_|\_\___|
#         __/ |
#        |___/
#
#
#------------------ Imports -------------------
import sys
import os
import subprocess
import thread
import time
import yaml
import re
import signal

try:
    import cPickle as pickle
except ImportError:  # python 3.x
    import pickle

from crontab import CronTab
from crontab import CronSlices

#<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-



#----------------- Variables -------------------
sshConfNameMapOld = {}
sshConfNameMapNew = {}

homedir = os.path.dirname(os.path.abspath(__file__))

varFolder = "/var/backupserver/"

backupRootFolder = '/srv/backups/'

clientsFolder = varFolder + "clients/"

hashFileName = homedir+"/.hash"

clientsYamlFile = varFolder + "clients.yml"

backupsYamlFile = varFolder + "backups.yml"

backupsYamlExampleFile = homedir+"/backups-example.yml"

clientsYamlExample = """clients:
  name1:
      sshHost: example.com
      sshPort: 22
      sshUser: user1

  name2:
      sshHost: example2.com
      sshPort: 2022
      sshUser: user2
 """
#<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-


#---------------- Main -----------------

def main():
    mkDir("/srv/backups")
    mkDir("/var/backupserver")

    initClientsFile()
    processConfiguration()

    initBackupsFile()
    checkBackupsYml()
    processBackupsYml()
    print "\nPress [Ctrl+C] to exit"
    while True:
        pass

#<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<


#---------------------- Handle Configuration -----------------------
def initClientsFile():

    if not os.path.exists(clientsFolder):
        if os.path.exists(hashFileName):
            os.remove(hashFileName)

    if not os.path.isfile(clientsYamlFile):
        f = open(clientsYamlFile, "w")
        f.write(clientsYamlExample)
        f.close()
        print "Example config file created!"
        nanoEditConfigFile()


def nanoEditConfigFile():
    os.system('nano ' + clientsYamlFile)
    while not query_yes_no("Editing completed?"):
        os.system('nano ' + clientsYamlFile)

def processConfiguration():
    global sshConfNameMapOld
    global sshConfNameMapNew

    rootElement = loadYamlFile(clientsYamlFile).get('clients')
    if rootElement is None:
      print "YAML File Error: root element 'clients' not found"
      exit()

    clients = rootElement.keys()

    print clients
    checkConfiguration(rootElement, clients)

    sshConfNameMapOld = readObject(hashFileName)

    if sshConfNameMapOld == None:
        sshConfNameMapOld = {}

    canceled = False

    for client in clients:
        connData = getConnectionDataFromClient(rootElement,client)
        sshConfNameMapNew[connData.getHash()] = client



        if connData.getHash() in sshConfNameMapOld.keys() and client == sshConfNameMapOld[connData.getHash()]:
            #No changes
            pass
        else:
            if connData.getHash() in sshConfNameMapOld.keys():
                print("mv "+clientsFolder+sshConfNameMapOld[connData.getHash()]+" "+clientsFolder+client)
                if os.path.exists(clientsFolder+client):
                    print("Path already exists! Path: "+clientsFolder+client)
                    exit()
                os.rename(clientsFolder+sshConfNameMapOld[connData.getHash()],clientsFolder+client)
            else:
                mkDir(clientsFolder+client)
                print "Create new client"
                createSshKey(client,connData)
                if not query_yes_no("No means back to the config file. Continue?"):
                    canceled = True
                    #os.remove(hashFileName)
                    nanoEditConfigFile()
                    processConfiguration()

    if not canceled:
        saveObject(hashFileName,sshConfNameMapNew)




def checkConfiguration(rootElement, clients):
    if len(clients) == 0:
        print "no clients in configuration"
        exit()

    sshConfNameMapOld = readObject(hashFileName)
    if sshConfNameMapOld == None:
        sshConfNameMapOld = {}

    #A set to check multiple identical configurations
    hashSet = set()

    for client in clients:
        connData = getConnectionDataFromClient(rootElement,client)
        connData.hostString = connData.getHostString()
        checkHostString(client,connData.hostString)
        checkPort(client,connData.sshPort)

        #Check Multiple Identical Configurations
        if connData.getHash() in hashSet:
            print "Same configuration multiple times found"
            print connData
            exit()
        else:
            hashSet.add(connData.getHash())

        #Check Renaming
        renaming = connData.getHash() in sshConfNameMapOld.keys() and client != sshConfNameMapOld[connData.getHash()]
        if renaming and os.path.exists(clientsFolder+client):
                print("Rename error! Path already exists! Path: "+clientsFolder+client)
                exit()



def checkPort(client,port):
    if port is None:
        print ("no sshPort set, client: "+client)
        exit()
    bool = re.match("[0-9]",str(port))
    if not bool:
        print ("no valid port: "+port+", client: "+client)
        exit()
    p = int(port)
    if p < 1 or p > 65535:
        print ("no valid port: "+port+", client: "+client)
        exit()

def checkHostString(client,hostString):
    if hostString is None:
        print "no sshHost set, client: "+client
        exit()
    bool = re.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",hostString)

    if bool:
        return
    else:
        print ("host string is invalid: "+hostString+", client: "+client)
        exit()



def createSshKey(name,connData):
  if os.path.exists(clientsFolder+name+'/.key'):
    os.remove(clientsFolder+name+'/.key')
  print ("Create ssh key for client '" + name + "'")
  os.system('ssh-keygen -f '+clientsFolder+name+'/.key -t rsa -b 4096 -q -N ""')
  os.system('ssh-copy-id -p '+str(connData.sshPort)+' -i '+clientsFolder+name+'/.key '+connData.getHostString());




#<-<-<-<-<-<-<-<-<-<-< Handle Configuration <-<-<-<-<-<-<-<-<-<-<



#--------------------- Handle Backup Config ---------------------



def initBackupsFile():
    if not os.path.isfile(backupsYamlFile):
        exampleF = open(backupsYamlExampleFile,"r")
        backupsYamlExample = exampleF.read()

        f = open(backupsYamlFile, "w")
        f.write(backupsYamlExample)
        f.close()
        print "Example backup file created!"
        os.system('nano '+backupsYamlFile)
        while not query_yes_no("Editing completed?"):
            os.system('nano '+backupsYamlFile)



def checkBackupsYml():
    confFile = loadYamlFile(clientsYamlFile)
    file = loadYamlFile(backupsYamlFile)
    rootElement = confFile.get('clients')

    defaultRmOlderThan = file.get('remove-older-than')
    defaultCron = file.get('cron');
    backups = file.get('backups');

    if not isinstance(backups, list):
        print "backups is not a list!"
        exit()


    for backup in backups:
        checkSingleBackup(backup, rootElement, defaultRmOlderThan, defaultCron)



def checkSingleBackup(backup, rootElement, defaultRmOlderThan, defaultCron):
    name = backup.get('name')
    client = backup.get('client')
    connData = getConnectionDataFromClient(rootElement,client)

    folder = backup.get('folder')
    cron = backup.get('cron')
    if cron is None:
        cron = defaultCron

    exclude = backup.get('exclude')

    include = backup.get('include')

    rmOlderThan = backup.get('remove-older-than')
    if rmOlderThan is None:
        rmOlderThan = defaultRmOlderThan

    # Start checking
    if folder is None:
        print "No folder set! Client: "+client+", Name: "+name
        exit()

    if cron is None:
        print "No cron set and no default cron set! Client: "+client+", Name: "+name
        exit()

    if not CronSlices.is_valid(cron):
        print "No valid cron! Client: "+client+", "+"Cron: "+cron+", Name: "+name
        exit()

    if exclude is not None:
        if not isinstance(exclude, list):
            print "exclude is not a list! Client: "+client+", Name: "+name
            exit()

    if include is not None:
        if not isinstance(include, list):
            print "include is not a list! Client: "+client+", Name: "+name
            exit()



def processBackupsYml():
    crontab = createCrontab()

    confFile = loadYamlFile(clientsYamlFile)
    file = loadYamlFile(backupsYamlFile)

    rootElement = confFile.get('clients')

    defaultRmOlderThan = file.get('remove-older-than')
    defaultCron = file.get('cron');
    backups = file.get('backups');

    backupjobs = []

    for backup in backups:
        backupjob = createSingleBackupJob(backup, rootElement, defaultRmOlderThan, defaultCron)
        backupjobs.append(backupjob)

    if query_yes_no_with_timeout("Do you want to test the backup jobs now?",15,False):
        for job in backupjobs:
            print ""
            printBackupJobInfos(job)
            if query_yes_no("Test Job? (No = skip)"):
                manualBackup(job)

    print "Create Cronjobs"

    for job in backupjobs:
        addCronjob(crontab,job.backupCron,job.backupCmd)
        if job.deleteCmd is not None:
            addCronjob(crontab,job.deleteCron,job.deleteCmd)

#    restartCrontab()
    print "Finish configure backups"



def asyncPrintMem(job,remoteMem):
    while not job.finished:
        mem = getMemory(job)
        status = "\rProcess: "+mem+" / " + remoteMem + "    "
        sys.stdout.write(status)
        sys.stdout.flush()
        time.sleep(1)



def manualBackup(job):

    mem = getRemoteMemory(job)

    status = "Process: 0 / " + mem + "    "
    sys.stdout.write(status)
    sys.stdout.flush()

    job.finished = False
    thread.start_new_thread(asyncPrintMem, (job, mem))

    result = os.system(job.backupCmd)

    job.finished = True

    if(result == 0):
        status = "Process successful finished! "+mem+"\n"
    else:
        status = "Process canceled! "
    sys.stdout.write("\r"+status)
    sys.stdout.flush()

    if job.deleteCmd is not None:
        os.system(job.deleteCmd)



def getMemory(backupjob):
    command = "du -h {--folder--} --exclude={--folder--}/rdiff-backup-data | tail -1"

    command = command.replace('{--folder--}', str(backupjob.folder))
    command = command.replace('//', '/')

    output = subprocess.check_output(command, shell=True)
    memory = output.split('\t')[0]
    return memory




def getRemoteMemory(backupjob):
    command = "ssh -o StrictHostKeyChecking=no -i {--clientsFolder--}/{--client--}/.key -p {--sshPort--} {--hostString--} 'du -h {--remoteFolder--} | tail -1'"

    command = command.replace('{--clientsFolder--}', str(clientsFolder))
    command = command.replace('{--client--}', str(backupjob.client))
    command = command.replace('{--sshPort--}', str(backupjob.connData.sshPort))
    command = command.replace('{--hostString--}', str(backupjob.connData.getHostString()))
    command = command.replace('{--remoteFolder--}', str(backupjob.remoteFolder))
    command = command.replace('{--folder--}', str(backupjob.folder))
    command = command.replace('//', '/')

    output = subprocess.check_output(command, shell=True)
    memory = output.split('\t')[0]
    return memory



def printBackupJobInfos(backupjob):
    print "Name: "+str(backupjob.name)
    print "Client: "+str(backupjob.client)
    print "Folder: "+str(backupjob.remoteFolder)
    print "Cron:   "+str(backupjob.backupCron)


def generateBackupFolder(backup):
    folder = backupRootFolder+backup.get('client')+'/'+backup.get('folder')
    folder = folder.replace('//','/')
    return folder






def createSingleBackupJob(backup, rootElement, defaultRmOlderThan, defaultCron):
    name = backup.get('name')
    client = backup.get('client')
    connData = getConnectionDataFromClient(rootElement,client)

    folder = generateBackupFolder(backup)
    remoteFolder = backup.get('folder')
    cron = backup.get('cron')
    if cron is None:
        cron = defaultCron
    exclude = backup.get('exclude')
    include = backup.get('include')
    rmOlderThan = backup.get('remove-older-than')
    if rmOlderThan is None:
        if defaultRmOlderThan is not None:
            rmOlderThan = defaultRmOlderThan
        else:
            rmOlderThan = "N"

    rdiffCmd = 'rdiff-backup --remote-schema "ssh -C -i {--clientsFolder--}/{--client--}/.key -p {--sshPort--} %s rdiff-backup --server" {--hostString--}::{--remoteFolder--} {--folder--}'

    rdiffCmd = rdiffCmd.replace('{--clientsFolder--}', clientsFolder)
    rdiffCmd = rdiffCmd.replace('{--client--}', client)
    rdiffCmd = rdiffCmd.replace('{--sshPort--}', str(connData.sshPort))
    rdiffCmd = rdiffCmd.replace('{--hostString--}', connData.getHostString())
    rdiffCmd = rdiffCmd.replace('{--remoteFolder--}', remoteFolder)
    rdiffCmd = rdiffCmd.replace('{--folder--}', folder)
    rdiffCmd = rdiffCmd.replace('//','/')

    if exclude is not None:
        for path in exclude:
            rdiffCmd += " --exclude "+path.get('path')

    if include is not None:
        for path in include:
            rdiffCmd += " --include "+path.get('path')

    print rdiffCmd

    rdiffDeleteCmd = 'rdiff-backup --remove-older-than {--rmOlderThan--} {--folder--}'

    rdiffDeleteCmd = rdiffDeleteCmd.replace('{--rmOlderThan--}', rmOlderThan)
    rdiffDeleteCmd = rdiffDeleteCmd.replace('{--client--}', client)
    rdiffDeleteCmd = rdiffDeleteCmd.replace('{--folder--}', folder)
    rdiffDeleteCmd = rdiffDeleteCmd.replace('//','/')

    print rdiffDeleteCmd

    backupjob = BackupJob()

    backupjob.name = name
    backupjob.backupCmd = rdiffCmd
    backupjob.backupCron = cron
    backupjob.client = client
    backupjob.folder = folder
    backupjob.remoteFolder = remoteFolder
    backupjob.connData = connData

    backupjob.deleteCron = cron

    if rmOlderThan != "N":
        backupjob.deleteCmd = rdiffDeleteCmd

    mkDir(folder)

    return backupjob



#<-<-<-<-<-<-<-<-<-<-< Handle Backup Config <-<-<-<-<-<-<-<-<-<-<



#----------------------------- Utils ----------------------------


def createCrontab():
    crontab = CronTab(user=True)
    crontab.remove_all()
    return crontab

def addCronjob(crontab,cron,cmd):
    job = crontab.new(command=cmd)
    job.setall(cron)
    crontab.write()


def restartCrontab():
    os.system("/etc/init.d/cron restart")



def getConnectionDataFromClient(rootElement, client):
    connData = ConnectionData()

    if rootElement.get(client) is None:
        print ("Client does not exist! Client: "+client)
        exit();

    connData.sshHost = rootElement.get(client).get('sshHost')
    connData.sshUser = rootElement.get(client).get('sshUser')
    connData.sshPort = rootElement.get(client).get('sshPort')
    return connData


def mkDir(name):
  if not os.path.exists(name):
    os.makedirs(name)


def readObject(file):
    if not os.path.exists(file):
        return

    with open(file, 'rb') as fp:
        object = pickle.load(fp)
        return object


def saveObject(file,object):
    with open(file, 'wb') as fp:
        pickle.dump(object, fp, protocol=pickle.HIGHEST_PROTOCOL)




def loadYamlFile(yamlFile):
    with open(yamlFile, 'r') as stream:
        try:
            obj = yaml.load(stream)
            return obj
        except yaml.YAMLError as exc:
            print(exc)
            exit()

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def query_yes_no_with_timeout(question,timeout,default):

    def interrupted(signum, frame):
        print ""
        return true

    def input(question, timeout,default):
        try:
            foo = "A"
            validY = ['y','ye','yes']
            validN = ['n','no']

            while foo not in validY and foo not in validN:
                if default:
                    default = 'y'
                else:
                    default = 'n'
                print '(['+default+'] in '+str(timeout)+'sec) '+ question + '[y/n]',
                foo = raw_input()
                if foo not in validY and foo not in validN:
                    print "No valid answer. Please respond with 'yes' or 'no' (or 'y' or 'n')"


            if foo in validY:
                return "true"
            if foo in validN:
                return "false"

            return None
        except:
            # timeout
            return

#def query_yes_no_with_timeout
    signal.signal(signal.SIGALRM, interrupted)
    signal.alarm(timeout)
    s = input(question, timeout, default)
    signal.alarm(0)


    if s == "true":
        return True
    if s == "false":
        return False
    if s == None:
        return default








#                  [J]
#---------------- POPO's --------------
# Plain Old Python Object
# I don't find a name for such python object's in the internet, so I decide to use POPO  :P


class BackupJob():
    name = None
    backupCmd = None
    backupCron = None
    deleteCmd = None
    deleteCron = None
    client = None
    folder = None
    remoteFolder = None
    connData = None


class ConnectionData():
    sshHost = None
    sshPort = None
    sshUser = None

    def getHostString(self):
        string = str(self.sshUser) + "@" + str(self.sshHost)
        return string

    def getHash(self):
        string = self.sshUser + self.sshHost + str(self.sshPort)
        return hash(string)

    def __str__(self):
        string = ""
        string += "sshHost = "+self.sshHost+"\n"
        string += "sshPort = "+str(self.sshPort)+"\n"
        string += "sshUser = "+self.sshUser+"\n"
        return string

#<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-<-


#------- Start the Program ----------
if __name__ == '__main__':
    main()
