# Backupserver by Luke
A short python script to manage backups for your clients over ssh

Software you need:
    rdiff-backup,
    openssh-server,
    nano,
    cron,
    python2.7,
	python-pip

Python Libraries you need:
    pyyaml,
    python-crontab,

## 1. Introduction
This script is designed for simple configuration over YAML files.

I programmed the script with good intention, but I cannot rule out that it contains errors or vulnerabilities.
If you have criticism, improvement suggestions or errors please send me a message.

The script is open to everyone and can be used by anyone.
If you like the product I am looking forward to any feedback.

## 2. Short Concept
1.  Prepare clients, create user and change permissions on your client
2.  Start the script
3.  Configure, add clients in file clients.yml 
4.  Configure, add backups in file backups.yml






## 3. Tutorial

### 3.1 First Run

Install the required software

Install the required python libraries with pip 

Start the Script

A file is opened automatically. You can configure your clients in that file.

*File: clients.yml*
```
clients:
  name1:
      sshHost: example.com
      sshPort: 22
      sshUser: user1

  name2:
      sshHost: example2.com
      sshPort: 2022
      sshPort: user2
```
Hint: Make a note of the names you enter for the clients, because you will need them later!

-------

Now you have to enter your passwords for the client ssh connections

*Output: process history*
```
. . .
. . .
Create new client
Create ssh key for client 'name1'
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/var/backupserver/clients/name1/.key.pub"
The authenticity of host '[example.com]:22 ([4.126.62.85]:22)' can't be established.
ECDSA key fingerprint is SHA256:/O7dpbQ05B49ynLpXNHptpvYsoa+teaAcyEyZj/tFh4.
Are you sure you want to continue connecting (yes/no)? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
user1@candoo.at's password:
```

Answer the question **Are you sure you want to continue connecting (yes/no)?** with **yes**

Then enter your password for the client. If you made a mistake during configuration you can end the password entry with [ctrl+c]. 

Now you will be asked if you want to continue or go back to the configuration file. Answer with **y** to continue or **n** if you made a mistake.

------------

Another file is opened automatically. In that file you can configure your backups.

*File: backups.yml*
```
remove-older-than: 6M
cron: 0 2 * * *
backups:
  - backup:
    name: A optional name
    client: name1
    folder: /var/www
    cron: 0 3 * * *
    exclude:
      - path: /var/www/temp
      - path: /var/www/logs
    include:
      - path: /var/www/temp/*.txt
    remove-older-than: 2W

  - backup:
    client: name1
    folder: /var/mysql

  - backup:
    client: name2
    folder: /var/myProgram
    cron: 0 3 * * *

```

|Field|Description|
|-------|----------------|
|remove-older-than|Sets the value when backups should be deleted. This is the default value for all backups. |
|cron|Sets the default execute trigger time for all backups |
|backup.name|Optional Name for a backup for debugging.|
|backup.client|Sets a client. Use a client name which you defined in the other file clients.yml|
|backup.folder|description|
|backup.cron|description|
|backup.exclude|description|
|backup.include|description|
|backup.remove-older-than|description|